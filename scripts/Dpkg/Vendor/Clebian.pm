# Copyright © 2024 Clevon AS <info@clevon.com>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
# REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
# AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
# INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
# LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
# OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.

=encoding utf8

=head1 NAME

Dpkg::Vendor::Clebian - Clebian vendor class

=head1 DESCRIPTION

This vendor class customizes the behavior of dpkg scripts for Clebian
specific behavior and policies.

B<Note>: This is a private module, its API can change at any time.

=cut

package Dpkg::Vendor::Clebian 0.01;

use strict;
use warnings;

use parent qw(Dpkg::Vendor::Debian);

sub run_hook {
	my ($self, $hook, @params) = @_;

	if ($hook eq 'archive-keyrings') {
		return ('/usr/share/keyrings/clevon-archive.pgp');
	} else {
		return $self->SUPER::run_hook($hook, @params);
	}
}

sub set_build_features {
	my ($self, $flags) = @_;

	$self->SUPER::set_build_features($flags);
	$flags->set_feature('hardening', 'branch', 0);
	$flags->set_feature('hardening', 'fortify', 0);
	$flags->set_feature('hardening', 'stackclash', 0);
	$flags->set_feature('hardening', 'stackprotector', 0);
	$flags->set_feature('hardening', 'stackprotectorstrong', 0);
	$flags->set_option_value('optimize-level', 3);
}

sub add_build_flags {
	my ($self, $flags) = @_;

	$self->SUPER::add_build_flags($flags);

	my @compile_flags = qw(
		CFLAGS
		CUDAFLAGS
		CXXFLAGS
	);
	$flags->append($_, '-DNDEBUG') foreach @compile_flags;

	$flags->append('LDFLAGS', '-Wl,--as-needed');
}

1;
